﻿using UnityEngine;
using System.Collections;

public class EPromptTrigger : MonoBehaviour 
{
    public GameObject ePromptPrefab;
    private bool activated = false;
    public Vector3 ePromptOffset;
    private Animator ePromptAnimator;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.Lerp(Color.blue, Color.white, 0.65f);
        Gizmos.DrawWireSphere(transform.position + ePromptOffset, 1f);
    }

    private GameObject ePromptInstance;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.GetComponent<StubbedPlatformer>())
        {
            if(!activated)
            {
                ePromptInstance = (GameObject)Instantiate(ePromptPrefab, transform.position + ePromptOffset, Quaternion.identity);
                ePromptAnimator = ePromptInstance.GetComponent<Animator>();
                activated = true;
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(activated && Input.GetKeyDown(KeyCode.E))
        {
            if(ePromptAnimator.GetBool("FadeOut"))
                ePromptAnimator.SetBool("FadeOut", true);
        }
    }
}