﻿using UnityEngine;
using System.Collections;

public class MusicTrigger : MonoBehaviour 
{
	public GameMusic music;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.GetComponent<StubbedPlatformer> ()) 
		{
			MusicManager.PlayMusic (music);
			Destroy (gameObject);
		}
	}
}