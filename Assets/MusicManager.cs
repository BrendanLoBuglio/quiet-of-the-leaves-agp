﻿using UnityEngine;
using System.Collections;

public enum GameMusic{SweetDad, DadsDead, None}

public class MusicManager : MonoBehaviour 
{
	private static MusicManager instance;
	private static MusicManager GetInstance() {if (instance == null)instance = FindObjectOfType<MusicManager> (); return instance;}

	private float fadeTarget = 1f;
	private float fadeLevel = 0f;
	public float fadeTime = 9f;

	public AudioClip sweetDadMusic;
	public AudioClip sadDadIsDeadMusic;
	public AudioClip targetMusic;
	private AudioSource source;

	void Start()
	{
		source = GetComponent<AudioSource> ();
	}

	public static void PlayMusic(GameMusic musicSelection)
	{
		GetInstance ();

		switch (musicSelection) 
		{
		case GameMusic.DadsDead:
			instance.targetMusic = instance.sadDadIsDeadMusic;
			break;
		case GameMusic.SweetDad:
			instance.targetMusic = instance.sweetDadMusic;
			break;
		case GameMusic.None:
			instance.targetMusic = null;
			break;
		}
	}

	public static void StopMusic()
	{
		GetInstance ().source.Stop ();
	}

	void Update () 
	{
		fadeTarget = (targetMusic == source.clip) ? 1f : 0f;

		fadeLevel += Mathf.Sign (fadeTarget - fadeLevel) * Time.deltaTime / fadeTime;

		if(fadeLevel <= 0f)
		{
			source.clip = targetMusic;
			source.Play ();
		}
		source.volume = fadeLevel;

		fadeLevel = Mathf.Clamp (fadeLevel, 0f, 1f);
	}
}
