﻿using UnityEngine;
using System.Collections;

public class EPromptTriggerWT : MonoBehaviour 
{
    public GameObject ePromptPrefab;
    public Vector3 ePromptOffset;
    private Animator ePromptAnimator;
	private bool activated = false;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.Lerp(Color.blue, Color.white, 0.65f);
        Gizmos.DrawWireSphere(transform.position + ePromptOffset, 1f);
    }

    private GameObject ePromptInstance;

	public void WalkieTalkieStartYoYo()
	{
     	if(!activated)
     	{
     	    ePromptInstance = (GameObject)Instantiate(ePromptPrefab, transform.position + ePromptOffset, Quaternion.identity);
     	    ePromptAnimator = ePromptInstance.GetComponent<Animator>();
			ePromptInstance.transform.SetParent (this.transform);
     	    activated = true;
     	}
    }

	void Update()
    {
        if(activated && Input.GetKeyDown(KeyCode.E))
        {
        	ePromptAnimator.SetBool("FadeOut", true);
        }
    }
}