﻿using UnityEngine;
using System.Collections;

public class AxeSwingZone : MonoBehaviour 
{
    private StubbedPlatformer player;
    public TreeController motherfuckinTree;
    public GameObject blockPlayer;

    public Animator         treeFallAnimator;
    public SpriteRenderer   treeFacadeRenderer;

    bool playerInBounds = false;

    void Start()
    {
        player = FindObjectOfType<StubbedPlatformer>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player.gameObject)
            playerInBounds = true;

        VirtualDad dad = other.gameObject.GetComponent<VirtualDad>();
        if (dad != null)
        {
            dad.SetPauseState(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player.gameObject)
            playerInBounds = false;
    }

    void Update()
    {
        if(playerInBounds)
        {
            player.CheckAxeSwing();
        }

        if(motherfuckinTree != null && motherfuckinTree.choppedAsFuck)
        {
            FindObjectOfType<VirtualDad>().SetPauseState(false);
            blockPlayer.SetActive(false);
			Destroy (this);
        }
    }
}