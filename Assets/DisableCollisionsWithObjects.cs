﻿using UnityEngine;
using System.Collections;

public class DisableCollisionsWithObjects : MonoBehaviour 
{
	public Collider2D[] objectsToIgnore;
	private Collider2D myCollider;

	void Start () 
	{
		myCollider = GetComponent<Collider2D> ();
		foreach (Collider2D c in objectsToIgnore) {
			Physics2D.IgnoreCollision (c, myCollider);
		}
	}	
}