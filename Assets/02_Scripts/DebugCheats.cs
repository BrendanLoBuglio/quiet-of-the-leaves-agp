﻿using UnityEngine;
using System.Collections;

public class DebugCheats : MonoBehaviour 
{
    private float shouldBeTimescale;


	void Update () 
    {
        if (Time.timeScale != 9f)
            shouldBeTimescale = Time.timeScale;

        if (Input.GetKey(KeyCode.Alpha1))
            Time.timeScale = 9f;
        else
            Time.timeScale = shouldBeTimescale;


		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			
			Destroy (FindObjectOfType<VirtualDad> ().gameObject);
			Destroy (FindObjectOfType<AxeSwingZone> ().gameObject);
		}
	}
}
