﻿using UnityEngine;
using System.Collections;

public class ContextUIManager : MonoBehaviour {

    private static ContextUIManager instance;
    private static ContextUIManager GetInstance() { if (instance == null) instance = FindObjectOfType<ContextUIManager>(); return instance; }
    public CanvasGroup walkieTalkieGroup;

	public Animator titleFadeInAnimator;
	public Animator escapeFadeInAnimator;

    void Start()
    {
        SetWalkieTalkieVisibility(false);
    }

    public static void SetWalkieTalkieVisibility(bool visibility)
    {
        GetInstance().walkieTalkieGroup.alpha = visibility ? 1f : 0f;
    }

	public static void FadeInTitle()
	{
		GetInstance(). titleFadeInAnimator.SetBool ("FadeIn", true);
	}

	public static void FadeInEscape()
	{
		GetInstance().escapeFadeInAnimator.SetBool ("FadeIn", true);
	}
}
