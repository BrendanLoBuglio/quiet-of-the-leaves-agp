﻿using UnityEngine;
using System.Collections;

public enum UpOrDown {Up, Down}

public class SwitchJoint : MonoBehaviour 
{
    public Collider2D upPath;
    public Collider2D downPath;

    void Start()
    {
        foreach (PathTraverser t in FindObjectsOfType<PathTraverser>())
        {
            SetPathIgnoreState(UpOrDown.Down, t);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        PathTraverser traverser = other.GetComponent<PathTraverser>();
        if(traverser != null)
        {
            traverser.DisablePathCollisions(downPath, false);

            if (other.GetComponent<VirtualDad>())
            {
                SetPathIgnoreState(UpOrDown.Up, traverser);
            }

            if (other.CompareTag("Player"))
            {
                if (traverser.ComingFromPath(downPath))
                    SetPathIgnoreState(UpOrDown.Up, traverser);
                else if(traverser.ComingFromPath(upPath))
                    SetPathIgnoreState(UpOrDown.Down, traverser);
            }
        }
    }

	void OnTriggerStay2D (Collider2D other) 
    {
	    PathTraverser traverser = other.GetComponent<PathTraverser>();
        if(traverser != null)
        {
            if (other.CompareTag("Player"))
            {
                if (Input.GetKey(KeyCode.UpArrow))
                    SetPathIgnoreState(UpOrDown.Up, traverser);
                if (Input.GetKey(KeyCode.DownArrow))
                    SetPathIgnoreState(UpOrDown.Down, traverser);
            }
        }
	}

    void SetPathIgnoreState(UpOrDown newTarget, PathTraverser traverser)
    {
        if(upPath != null)
        {
            bool disable = newTarget == UpOrDown.Down;
            traverser.DisablePathCollisions(upPath, disable);
        }
    }
}

//[System.Serializable]
//public class SwitchPath
//{
//    public GameObject pathObj;
//    public 
//}