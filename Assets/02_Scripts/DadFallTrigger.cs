﻿using UnityEngine;
using System.Collections;

public class DadFallTrigger : MonoBehaviour 
{
    private AudioSource audioSource;
    public AudioClip dadFallSequence;
    private VirtualDad dad;

	public GameObject samSprite;
	private Animator samAnimator;
	public GameObject forestAudio;
	public AudioSource dadSource;

	public GameObject[] objectsToTurnOnOnFall;
	public GameObject[] objectsToTurnOffOnFall;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
		samAnimator = samSprite.GetComponent<Animator> ();
		foreach (GameObject g in objectsToTurnOnOnFall)
			g.SetActive (false);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("DadfallTrigger hit something");
        if(other.GetComponent<VirtualDad>())
        {
            Debug.Log("To be more specific, DadfallTrigger hit Dad!");
            dad = other.GetComponent<VirtualDad>();
            StartCoroutine(DadFallSequence());


        }
    }

    IEnumerator DadFallSequence()
    {
		FindObjectOfType<VirtualDad> ().SetPauseState (true);
		while (dadSource.isPlaying)
			yield return null;
        yield return new WaitForSeconds(2f);
		FindObjectOfType<FadeManager> ().animationSpeed = 0.15f;
		FadeManager.FadeToBlack();
		MusicManager.PlayMusic (GameMusic.None);
        yield return new WaitForSeconds(8f);
		forestAudio.SetActive (false);
        audioSource.clip = dadFallSequence;
		audioSource.Play ();

		Destroy(dad.gameObject);
		samAnimator.SetBool ("samIsSleeping", true);

		foreach (GameObject g in objectsToTurnOnOnFall)
			g.SetActive (true);
		foreach (GameObject g in objectsToTurnOffOnFall)
			g.SetActive (false);

		while (audioSource.isPlaying)
			yield return null;

		Destroy (FindObjectOfType<DayNightManager> ().gameObject);
		FindObjectOfType<FadeManager> ().animationSpeed = 0.1f;
        FadeManager.FadeFromBlack();
		FindObjectOfType<StubbedPlatformer> ().FreezeMovement (10f);
		samAnimator.SetBool ("samIsSleeping", false);
		forestAudio.SetActive (true);

    }
}
