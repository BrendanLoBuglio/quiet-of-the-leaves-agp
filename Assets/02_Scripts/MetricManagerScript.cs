﻿using UnityEngine;
using System.Collections;
using System.IO;

public class MetricManagerScript : MonoBehaviour {

	string createText = "";

	public int playerJumpMetric = 0;
	public int playerAxeHitMetric = 0;
	public int playerAxeMissMetric = 0;
	public int playerAxeSwingMetric = 0;
	public int playerScaredBirdMetric = 0;

	void Start () {}
	void Update () {

		playerAxeMissMetric = playerAxeSwingMetric - playerAxeHitMetric;

	}

	//When the game quits we'll actually write the file.
	void OnApplicationQuit(){
		GenerateMetricsString ();
		string time = System.DateTime.UtcNow.ToString ();string dateTime = System.DateTime.Now.ToString (); //Get the time to tack on to the file name
		print("before: " + time);
		time = time.Replace ("/", "-"); // Replace slashes with dashes, because Unity thinks they are directories..
		time = time.Replace (":", "-"); // Replace colons with dashes
		time = time.Replace (" ", "__"); // Replace white space with two underscores
		string reportFile = "GameName_Metrics_" + time + ".txt"; 
		createText = createText.Replace("\n", System.Environment.NewLine); // Replace newline characters with the system's newline representation.
		File.WriteAllText (reportFile, createText);
		//In Editor, this will show up in the project folder root (with Library, Assets, etc.)
		//In Standalone, this will show up in the same directory as your executable
	}

	void GenerateMetricsString(){
		createText = 
			"OH MY GOSH YOU BETTER GET READY FOR SOME NUMBERS!?!" + "\n" +
			"NUMBERS ARE SOOOOO FUN!!" + "\n" +
			"\n" +
			"YAAAAaaaaAAAY StaTIstICsS!" + "\n" +
			"\n" +
			"@______@ lol#swagdope" + "\n" +
			"\n" +
			"---------------------------------" + "\n" +
			"AND HERE THEY ARE!" + "\n" +
			"Number of times the player jumped: " + playerJumpMetric + "\n" +
			"Number of times the player hit with the axe: " + playerAxeHitMetric + "\n" +
			"Number of times the player missed with the axe: " + playerAxeMissMetric + "\n" +
			"Number of times the player scared a bird: " + playerScaredBirdMetric;


	}

	//Add to your set metrics from other classes whenever you want
	public void AddToPlayerJumpMetric(){
		playerJumpMetric += 1;
	}

	public void AddToPlayerAxeHitMetric(){
		playerAxeHitMetric += 1;
	}

	public void AddToPlayerAxeSwingMetric(){
		playerAxeSwingMetric += 1;
	}

	public void AddToPlayerScaredBirdMetric(){
		playerScaredBirdMetric += 1;
	}

}

