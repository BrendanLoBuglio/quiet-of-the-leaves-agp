﻿using UnityEngine;
using System.Collections;

public class StubbedPlatformer : MonoBehaviour 
{
    public float walkSpeed = 5f;
	float playerPositionX;
    private bool movementEnabled;
    private bool paused;

	public MetricManagerScript metricManagerScript;

	bool onRaft = false;

    Rigidbody2D rgbd2D;
   // public Transform spriteChild;
	public GameObject spriteChild;
	public GameObject awfulTestAxe;
	public GameObject raft;
	private Animator animator;
	private BoxCollider2D axeBoxCollider2D;
	private bool samIsSwingingAxe = false;
	public GameObject pressATrigger;
    private Coroutine finishSwingSequence;

	void Start () 
    {
        rgbd2D = GetComponent<Rigidbody2D>();

		animator = spriteChild.GetComponent<Animator> ();

		axeBoxCollider2D = awfulTestAxe.GetComponent<BoxCollider2D> ();
        metricManagerScript = FindObjectOfType<MetricManagerScript>();
	}
	
	void Update () 
    {
        if (movementEnabled && !paused)
        {

            rgbd2D.velocity = new Vector2(walkSpeed * Input.GetAxis("Horizontal"), rgbd2D.velocity.y);
            animator.speed = Mathf.Clamp01(Mathf.InverseLerp(0f, walkSpeed, Mathf.Abs(rgbd2D.velocity.x)) + 0.25f);
        }
        else
        {
            rgbd2D.velocity = new Vector2(0f, rgbd2D.velocity.y / 2f);
            animator.speed = 1f;
        }

        if(Mathf.Abs(Input.GetAxisRaw("Horizontal")) <= 0.05f)
            animator.speed = 1f;
        
		if (rgbd2D.velocity.x > 0.01f && movementEnabled) 
        {	
			spriteChild.transform.localScale = new Vector2 (0.4f, 0.4f);
			animator.SetBool ("samIsWalking", true);
            if(axeBoxCollider2D != null)
			    axeBoxCollider2D.offset = new Vector2 (-2.16f, 0.64f);

        }
        else if (rgbd2D.velocity.x < -0.01f && movementEnabled)
        {
			spriteChild.transform.localScale = new Vector2 (-0.4f, 0.4f);
			animator.SetBool ("samIsWalking", true);
            if (axeBoxCollider2D != null)
			    axeBoxCollider2D.offset = new Vector2 (2.16f, 0.64f);

		} else if (rgbd2D.velocity.x == 0f) {
			animator.SetBool ("samIsWalking", false);
		}

		if (onRaft == true) {
			animator.SetBool ("samIsWalking", false);
		}

        awfulTestAxe.SetActive(samIsSwingingAxe);

		//if (Input.GetKeyDown (KeyCode.E)) {
		//}
	}

	public void WalkieSequence()
	{
		animator.SetBool ("samGrabsWalkie", true);

		FreezeMovement (9999f);
		Pause (true);
	}
    public void Pause(bool pause)
    {
        paused = pause;
    }

    public void FreezeMovement(float freezeTime)
    {
        StartCoroutine(FreezeSequence(freezeTime));
    }

    IEnumerator FreezeSequence(float freezeTime)
    {
        movementEnabled = false;
        yield return new WaitForSeconds(freezeTime);
        movementEnabled = true;
    }

    private bool chopTree = false;

    public void CheckAxeSwing()
    {
        if (Input.GetKeyDown(KeyCode.E) && samIsSwingingAxe == false)
        {
            //Debug.Log ("should call coroutine");
            samIsSwingingAxe = true;
            animator.SetBool("samSwingsAxe", true);
            if (metricManagerScript != null)
                metricManagerScript.AddToPlayerAxeSwingMetric();
            
            if (finishSwingSequence != null)
                StopCoroutine(finishSwingSequence);
            finishSwingSequence = StartCoroutine(FinishAxeSwing());
        }
    }

	IEnumerator FinishAxeSwing()
	{
		yield return new WaitForSeconds(1f);
		samIsSwingingAxe = false;
		animator.SetBool ("samSwingsAxe", false);
	}

	void OnTriggerEnter2D (Collider2D triggeredObject){
		if (triggeredObject.tag == "rafttrigger") {
			//Debug.Log ("on raft trigger");
			onRaft = true;
		}

		if (triggeredObject.tag == "pressatrigger") {
			pressATrigger.SetActive (true);
		}
	}

	void OnTriggerExit2D (Collider2D triggeredObject){
		if (triggeredObject.tag == "rafttrigger") {
			onRaft = false;
		}
		if (triggeredObject.tag == "pressatrigger") {
			pressATrigger.SetActive (false);
		}
	}

}