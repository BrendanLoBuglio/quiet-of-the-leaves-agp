﻿using UnityEngine;
using System.Collections;

public class DayNightManager : MonoBehaviour 
{
    public float nightStartHeight   = 145f;
    public float nightMaxHeight     = 185f;
    public float nightFadeOutHeight  = 225f;
    public float sunsetTime         = 20f;
    private float sunsetTimer       = 0f;
    private float heightIndex       = 0f;

    public SpriteRenderer darknessFilter;
    public SpriteRenderer darkSky;
    public SpriteRenderer sunsetGradient;
    public Color maxSunsetColor;
    public Color maxDarkSkyColor;
    public Color maxDarknessColor;
    public Transform player;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.Lerp(Color.blue, Color.red, 0.2f);
        Gizmos.DrawWireCube(new Vector3(transform.position.x, nightStartHeight), 2.5f * Vector3.one);

        Gizmos.color = Color.Lerp(Color.blue, Color.red, 0.5f);
        Gizmos.DrawWireCube(new Vector3(transform.position.x, nightMaxHeight), 2.5f * Vector3.one);

        Gizmos.color = Color.Lerp(Color.blue, Color.red, 0.8f);
        Gizmos.DrawWireCube(new Vector3(transform.position.x, nightFadeOutHeight), 2.5f * Vector3.one);
    }

	void Update () 
    {
        if (player.transform.position.y > heightIndex)
            heightIndex = player.transform.position.y;
        if (heightIndex > nightMaxHeight)
            sunsetTimer += Time.deltaTime;
        
        float nightFadeT = 0f;
        if(heightIndex < nightMaxHeight)
            nightFadeT = Mathf.Clamp01(Mathf.InverseLerp(nightStartHeight, nightMaxHeight, heightIndex));
        else
            nightFadeT = Mathf.Clamp01(Mathf.InverseLerp(nightFadeOutHeight, nightMaxHeight, heightIndex));

        float sunsetT = Mathf.Clamp01(Mathf.InverseLerp(0f, sunsetTime, sunsetTimer));

        darknessFilter.color = new Color(maxDarknessColor.r, maxDarknessColor.g, maxDarknessColor.b, nightFadeT * maxDarknessColor.a);
        darkSky.color = new Color(maxDarkSkyColor.r, maxDarkSkyColor.g, maxDarkSkyColor.b, nightFadeT * maxDarkSkyColor.a);
        sunsetGradient.color = new Color(maxSunsetColor.r, maxSunsetColor.g, maxSunsetColor.b, (sunsetTimer / sunsetTime) * maxSunsetColor.a);
	}
}