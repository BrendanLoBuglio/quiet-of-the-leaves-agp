﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum MenuState {MainLayer, CreditsLayer, Exiting}

public class MenuManager : MonoBehaviour 
{
    public Text newGame;
    public Text credits;
    public Text exitGame;
    public Image menuCursor;
    private int menuIndex = 0;
    public float cursorLeftOffset = 60f;
    public float cursorUpOffset = -10f;
    private MenuState currentState = MenuState.MainLayer;
    public CanvasGroup creditsGroup;
    private AudioSource source;
    public AudioClip[] menuPops;
    private int soundIndex = 0;

    void Start()
    {
        source = GetComponent<AudioSource>();
        Cursor.visible = false;
    }

	void Update () 
    {
        switch(currentState)
        {
            case MenuState.MainLayer:
                creditsGroup.alpha = 0f;

                if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    menuIndex--;
                    WoodPop();
                }
                if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.RightArrow))
                {
                    menuIndex++;
                    WoodPop();
                }
                if (menuIndex < 0)
                    menuIndex = 2;
                if (menuIndex > 2)
                    menuIndex = 0;

                menuCursor.rectTransform.anchoredPosition = new Vector2(MenuItemByIndex(menuIndex).rectTransform.rect.min.x + cursorLeftOffset, MenuItemByIndex(menuIndex).rectTransform.anchoredPosition.y + cursorUpOffset);

		        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                {
                    switch(menuIndex)
                    {
                        case 0:
                            WoodPop();
                            currentState = MenuState.Exiting;
                            StartCoroutine(NextLevelSequence());
                            break;
                        case 1:
                            WoodPop();
                            currentState = MenuState.CreditsLayer;
                            break; 
                        case 2:
                            WoodPop();
                            currentState = MenuState.Exiting;
                            StartCoroutine(ExitSequence());
                            break;
                        default:
                            break;
                    }
		        }
            break;
            case MenuState.CreditsLayer:
                creditsGroup.alpha = 1f;
                if (Input.anyKeyDown)
                {
                    WoodPop();
                    currentState = MenuState.MainLayer;
                }
            break;
            default: break;
        }
	}

    IEnumerator NextLevelSequence()
    {
        FadeManager.FadeToBlack();
        yield return new WaitForSeconds(4.25f);
        Application.LoadLevel(1);
    }

    IEnumerator ExitSequence()
    {
        FadeManager.FadeToBlack();
        yield return new WaitForSeconds(4.25f);
        Application.Quit();
    }

    void WoodPop()
    {
        source.PlayOneShot(menuPops[soundIndex]);
        soundIndex++;
        if (soundIndex >= menuPops.Length)
        {
            soundIndex = 0;
        }
    }

    private Text MenuItemByIndex(int index) 
    {
        switch(index)
        {
            case 0: return newGame;
            case 1: return credits;
            case 2: return exitGame;
            default: return newGame;
        }
    }
}
