﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Transform player;
    public Transform dad;
    private Camera camera;

    public float cliffZoomLowestY;
    public float cliffZoomHighestY;
    public float baseOrthoSize = 6.5f;
    public float zoomedOutOrthoSize = 15f;
    public Vector3 positionOffset = new Vector3(0f, 3f, -20f);
    public float maxDadDistance = 5f;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.Lerp(Color.red, Color.yellow, 0.75f);
        Gizmos.DrawWireSphere(new Vector3(transform.position.x, cliffZoomLowestY), 2f);
        Gizmos.color = Color.Lerp(Color.red, Color.yellow, 0.25f);
        Gizmos.DrawWireSphere(new Vector3(transform.position.x, cliffZoomHighestY), 2f);
    }
	
    void Start()
    {
        camera = GetComponent<Camera>();
    }

	void Update () 
    {
        Vector3 centerPosition = Vector3.zero;
        if (dad != null)
            centerPosition = dad.position + Vector3.ClampMagnitude(player.position - dad.position, maxDadDistance);
        else
            centerPosition = player.position;

        transform.position = centerPosition + positionOffset;
        float targetOrthoSize = Mathf.Lerp(baseOrthoSize, zoomedOutOrthoSize, Mathf.InverseLerp(cliffZoomLowestY, cliffZoomHighestY, player.transform.position.y));
        camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, targetOrthoSize, 0.1f);
	}
}
