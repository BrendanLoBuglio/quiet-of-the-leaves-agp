﻿using UnityEngine;
using System.Collections;

public class EndSceneTrigger : MonoBehaviour 
{
  

    void Start()
    {
        
    }
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<StubbedPlatformer>())
        {
            StartCoroutine(EndScene());
        }
    }

    IEnumerator EndScene()
    {
        
       
        FadeManager.FadeToBlack();
		yield return new WaitForSeconds (2f);
		Application.Quit();
    }

}
