﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour 
{
    private CanvasGroup cGroup;
    private bool isPaused = false;
    private int menuIndex;
    public Text resumeGame;
    public Text returnToMenu;
    public Image menuCursor;
    public float cursorLeftOffset = -60;
    public float cursorUpOffset = 0f;
    public AudioClip[] menuPops;
    private AudioSource source;
    private int soundIndex = 0;

	void Start () 
    {
        cGroup = GetComponent<CanvasGroup>();
        source = GetComponent<AudioSource>();
	}
	
	void Update () 
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
            WoodPop();
        }

        if(isPaused)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                WoodPop();
                menuIndex++;
                if (menuIndex >= 2)
                    menuIndex = 0;
            }
            if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                if(menuIndex == 0)
                {
                    isPaused = false;
                    WoodPop();
                }
                if(menuIndex == 1)
                {
                    WoodPop();
                    FadeManager.FadeToBlack();
                    StartCoroutine(Exit());
                }
            }
        }

        

        menuCursor.rectTransform.anchoredPosition = new Vector2(resumeGame.rectTransform.rect.min.x + cursorLeftOffset, (menuIndex == 0 ? resumeGame.rectTransform.anchoredPosition.y : returnToMenu.rectTransform.anchoredPosition.y) + cursorUpOffset);

        cGroup.alpha = isPaused ? 1f : 0f;
	}

    void PauseGame(bool newPaused)
    {
        isPaused = newPaused;
        if(newPaused == true)
        {
            FindObjectOfType<StubbedPlatformer>().Pause(true);

            foreach(AudioSource s in FindObjectsOfType<AudioSource>())
            {
                s.Pause();
            }
        }
        else
        {
            FindObjectOfType<StubbedPlatformer>().Pause(false);
            foreach (AudioSource s in FindObjectsOfType<AudioSource>())
            {
                s.UnPause();
            }
        }
    }

    void WoodPop()
    {
        source.PlayOneShot(menuPops[soundIndex]);
        soundIndex++;
        if (soundIndex >= menuPops.Length)
        {
            soundIndex = 0;
        }
    }

    IEnumerator Exit()
    {
        yield return new WaitForSeconds(5.5f);
        Application.LoadLevel(0);
    }
}
