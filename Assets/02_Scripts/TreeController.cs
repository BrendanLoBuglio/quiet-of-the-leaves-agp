﻿using UnityEngine;
using System.Collections;

public class TreeController : MonoBehaviour 
{
	public Animator treeFallAnimator;
	public SpriteRenderer treeFacadeRenderer;
	public MetricManagerScript metricManagerScript;
    public Sprite[] treeChopSprites;
    private int treeChopIndex = 0;
    public bool choppedAsFuck = false;
	private AudioSource source;
	public AudioClip[] treeChops;
	public AudioClip[] samGrunts;
	public AudioClip treeFall;

	void Start()
	{
		source = GetComponent<AudioSource> ();
	}


    void ChopTree()
    {
        treeChopIndex++;
		source.PlayOneShot (treeChops[Random.Range(0, treeChops.Length)]);
        treeFacadeRenderer.sprite = treeChopSprites[Mathf.Clamp(treeChopIndex, 0, treeChopSprites.Length - 1)];
        if(treeChopIndex >= treeChopSprites.Length)
        {
            treeFacadeRenderer.enabled = false;
            treeFallAnimator.SetBool("theTreeMustFall", true);
			source.PlayOneShot (treeFall);
            Invoke("SetTreeToBeJustPlainFuckingChopped", 4.5f);
        }
    }

	void OnTriggerEnter2D (Collider2D triggeringObject)
    {
		//Debug.Log ("something hit the tree");
		if (triggeringObject.gameObject.tag == "axe") 
        {
			source.PlayOneShot (samGrunts [Random.Range (0, samGrunts.Length)]);

            Invoke("ChopTree", 0.8f);

			Debug.Log ("Axe hit tree: " + triggeringObject.name);
			if(metricManagerScript != null)
                metricManagerScript.AddToPlayerAxeHitMetric ();
		}
	}

    void SetTreeToBeJustPlainFuckingChopped()
    {
        choppedAsFuck = true;
    }
}
