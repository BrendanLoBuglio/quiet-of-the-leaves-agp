﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {

	private Animator animator;
	public GameObject player;
	public MetricManagerScript metricManagerScript;
	private AudioSource source;
	public AudioClip flyAwaySound;

	bool timeToFlyAway = false;
	bool playedSound = false;

	float distanceFromPlayer;

	// Use this for initialization
	void Start () {
	
		animator = gameObject.GetComponent<Animator> ();
		source = GetComponent<AudioSource> ();
        metricManagerScript = FindObjectOfType<MetricManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
		distanceFromPlayer = Vector3.Distance (player.transform.position, gameObject.transform.position);

		if (distanceFromPlayer <= 8f) 
		{
			animator.SetBool ("isScared", true);
			timeToFlyAway = true;
			if (!playedSound) 
			{
				playedSound = true;
				source.pitch = Random.Range (1.4f, 1.8f);
				source.PlayOneShot (flyAwaySound, 0.4f);
			}

            if(metricManagerScript != null)
			    metricManagerScript.AddToPlayerScaredBirdMetric ();
		}

		if (timeToFlyAway == true) 
		{
			Vector3 randomDirection = new Vector3(10f, Random.Range(0f, 10f));
			transform.Translate (randomDirection * Time.deltaTime);
		}

	}


}
