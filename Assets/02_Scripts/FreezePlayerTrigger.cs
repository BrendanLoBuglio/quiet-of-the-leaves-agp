﻿using UnityEngine;
using System.Collections;

public class FreezePlayerTrigger : MonoBehaviour 
{
    void OnTriggerEnter2D(Collider2D other)
    {
        StubbedPlatformer player = other.GetComponent<StubbedPlatformer>();
        if(player != null)
        {
            player.FreezeMovement(11.5f);
            Destroy(gameObject);
        }
    }
}
