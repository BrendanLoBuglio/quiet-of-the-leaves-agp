﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DadConversationManager : MonoBehaviour 
{
    public AudioSource source;
    private Queue<AudioClip> dadConversationSequence;

	void Start () 
    {
        source = GetComponent<AudioSource>();
        dadConversationSequence = new Queue<AudioClip>();
	}
	
    void Update()
    {
        if(!source.isPlaying && dadConversationSequence.Count > 0)
        {
            source.clip = dadConversationSequence.Dequeue();
            source.Play();
        }
    }

	void OnTriggerEnter2D (Collider2D other) 
    {
        DadConversationNode node = other.GetComponent<DadConversationNode>();
	    if(node != null)
        {
			foreach(AudioClip c in node.conversationBeats)
            	dadConversationSequence.Enqueue(c);
            Destroy(other.gameObject);
        }
	}
}