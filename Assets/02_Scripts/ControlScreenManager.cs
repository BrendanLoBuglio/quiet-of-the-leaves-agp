﻿using UnityEngine;
using System.Collections;

public class ControlScreenManager : MonoBehaviour 
{
    void Start()
    {
        Invoke("FadeOut", 6f);
        Invoke("NextScene", 10.5f);
    }

	
    void FadeOut()
    {
        FadeManager.FadeToBlack();
    }

    void NextScene () 
    {
        Application.LoadLevel(2);
	}
}
