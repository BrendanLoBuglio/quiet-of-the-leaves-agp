﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathTraverser : MonoBehaviour 
{
    private Collider2D collider;
    private List<Collider2D> lastCollidedPaths;

    void Awake()
    {
        collider = GetComponent<Collider2D>();
        lastCollidedPaths = new List<Collider2D>();
    }

    public bool ComingFromPath(Collider2D comparison)
    {
        return lastCollidedPaths.Contains(comparison);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.GetComponent<Path>())
        {
            if(!lastCollidedPaths.Contains(other.collider))
            {
                lastCollidedPaths.Add(other.collider);
                if(lastCollidedPaths.Count > 1)
                    lastCollidedPaths.RemoveAt(0);
            }
        }
    }

    public void DisablePathCollisions(Collider2D pathToIgnore, bool ignore)
    {
        Physics2D.IgnoreCollision(collider, pathToIgnore, ignore);
        Debug.Log("Collisions between " + gameObject.name + " and " + pathToIgnore.name + " are " + (ignore ? "Disabled!" : "Enabled!"));
    }
}
