﻿using UnityEngine;
using System.Collections;

public class VirtualDad : MonoBehaviour 
{
    public float walkSpeed = 4.5f;
    public SwitchJoint[] jointTrail;
    int targetJointIndex = -1;
    Rigidbody2D rgb;
    private Vector2 walkVel;
    private StubbedPlatformer myBelovedDaughter;
    public AnimationCurve walkSpeedFalloff;
    private Animator dadAnimator;
    private bool paused = false;

    void Start()
    {
        myBelovedDaughter = FindObjectOfType<StubbedPlatformer>();
        rgb = GetComponent<Rigidbody2D>();
        targetJointIndex = -1;
        GoToNextJoint();
        dadAnimator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        float speedFactor = paused ? 0f : 1f * walkSpeedFalloff.Evaluate(Mathf.InverseLerp(10f, 3f, Vector2.Distance(myBelovedDaughter.transform.position, transform.position)));

        if(speedFactor >= 0.3f)
        {
            rgb.velocity = new Vector2(walkVel.x * speedFactor, rgb.velocity.y);
            transform.localScale = new Vector3(Mathf.Sign(rgb.velocity.x), 1f, 1f);
            dadAnimator.SetBool("Walking", true);
            dadAnimator.speed = Mathf.Clamp01(speedFactor * 1.5f);
        }
        else
        {
            rgb.velocity = new Vector2(0f, rgb.velocity.y);
            transform.localScale = new Vector3(Mathf.Sign(myBelovedDaughter.transform.position.x - transform.position.x), 1f, 1f);
            dadAnimator.SetBool("Walking", false);
        }
    }

    public void SetPauseState(bool pauseStateIn)
    {
        paused = pauseStateIn;
    }

    void GoToNextJoint()
    {
        targetJointIndex++;
        walkVel = (jointTrail[targetJointIndex].transform.position.x - transform.position.x > 0 ? 1f : -1f) * Vector2.right * walkSpeed;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<SwitchJoint>() == jointTrail[targetJointIndex])
        {
            GoToNextJoint();
        }
    }
}
