﻿using UnityEngine;
using System.Collections;

public class DadCallTrigger : MonoBehaviour 
{
    private AudioSource source;
    public AudioClip staticIntro;
	public AudioClip staticLooping;
	public AudioClip dadDialogue;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<StubbedPlatformer>())
        {
            StartCoroutine(DadCall());
        }
    }

    IEnumerator DadCall()
    {
		MusicManager.StopMusic ();
        source.clip = staticIntro;
        source.Play();
		source.loop = false;
		while (source.isPlaying)
			yield return null;
		source.clip = staticLooping;
		source.loop = true;
		source.Play ();
		ContextUIManager.SetWalkieTalkieVisibility (true);
        while(!Input.GetKey(KeyCode.E))
        {
            yield return null;
        }
		FindObjectOfType<StubbedPlatformer> ().WalkieSequence ();

        ContextUIManager.SetWalkieTalkieVisibility(false);
		source.Stop ();
		source.clip = dadDialogue;
		source.loop = false;
		source.Play ();
		yield return new WaitForSeconds(3f);
		FindObjectOfType<FadeManager> ().animationSpeed = 999f;
		FadeManager.FadeToBlack();
		yield return new WaitForSeconds (3.5f);
		ContextUIManager.FadeInTitle ();
		yield return new WaitForSeconds (11f);
		ContextUIManager.FadeInEscape ();

		while (!Input.GetKey (KeyCode.Escape))
			yield return null;
		
		Application.LoadLevel (0);
    }
}
