﻿using UnityEngine;
using System.Collections;

public class DadConversationNode : MonoBehaviour 
{
    void OnDrawGizmos()
    {
        Gizmos.color = Color.Lerp(Color.magenta, Color.yellow, 0.4f);
        Gizmos.DrawWireSphere(transform.position, 0.75f);
    }

    public AudioClip[] conversationBeats;
}
