﻿using UnityEngine;
using System.Collections;

public class CameraBoundaryManager : MonoBehaviour 
{
    public Camera cam;
    public BoxCollider2D leftBoundary;
    public BoxCollider2D rightBoundary;
    public BoxCollider2D lowerBoundary;

	void Update () 
    {
        leftBoundary.transform.localPosition   = -Vector3.right * (1.10f * cam.orthographicSize * cam.aspect + leftBoundary.bounds.extents.x);
		rightBoundary.transform.localPosition  =  Vector3.right * (1.10f * cam.orthographicSize * cam.aspect + rightBoundary.bounds.extents.x);
        lowerBoundary.transform.localPosition  = -Vector3.up    * (1.33f * cam.orthographicSize + lowerBoundary.bounds.extents.y);
	}
}
